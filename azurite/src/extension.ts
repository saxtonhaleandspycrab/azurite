// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
const util = require('util');


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext) {
	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "helloworld-sample" is now active!');
	let workspaceRoot = vscode.workspace.rootPath
	let defaultPath = workspaceRoot;

	if (!workspaceRoot) {
		return;
	}
	let AzuritePath = ".\\Azurite.exe";
	const out = vscode.window.createOutputChannel("Export output");


	vscode.commands.registerCommand('azurite.convert', async config => {
		const editor = vscode.window.activeTextEditor;

		if (editor == undefined)
			return vscode.window.showErrorMessage("You need to focus a window in order to convert");

		let filename = editor.document.fileName;

		// Ask for Azurite path
		const tempPath = await vscode.window.showInputBox({
			placeHolder: "please precise the path of the Azurite exe",
			value: AzuritePath
		});

		if (tempPath == undefined)
			return vscode.window.showErrorMessage("please provide a path");
		AzuritePath = tempPath;
		
		const execFile = util.promisify(require('child_process').execFile);
		
		// Ask Azurite the language of the current file
		const choice = await execFile(AzuritePath, [filename, "-l"]).then((dat: any) => {
			if (dat) {
				const lines = dat.stdout.split("\n");
				return lines[lines.length - 1].split(",");
			}
		})


		// Ask in wichs languages the folder should be translated
		const lang = await vscode.window.showQuickPick(choice, {
			canPickMany: true
		});

		if (lang == undefined)
			return vscode.window.showErrorMessage("you need to select at least on language");

		// Get the path for the export
		var exportPath = defaultPath;
		filename = filename.replace(/^.*[\\\/]/, '');
		filename = filename.slice(0, filename.length - 5);
		exportPath += "\\" + filename
		if (exportPath == undefined)
			return vscode.window.showErrorMessage("can't find an export path");

		// Final step: Execute the command
		const parameters = [editor.document.uri.fsPath, "-t", ...lang, "-s", exportPath].concat(lang);

		const child = execFile(AzuritePath, parameters, (error: any, stdout: any, stderr: any) => {
			if (error) {
				out.appendLine(error);
				throw error;
			}
			out.appendLine(stdout);
			out.appendLine(stderr);
		});


	});

	vscode.commands.registerCommand("azurite.setDefaultPath", async config => {
		const result = await vscode.window.showOpenDialog({
			canSelectFiles: false,
			canSelectFolders: true,
		}).then(folderUri => {
			if (folderUri && folderUri[0])
				return folderUri[0].fsPath;
		});
		if (result == undefined)
			return vscode.window.showErrorMessage("please select a folder");
		defaultPath = result;
	});
}

// this method is called when your extension is deactivated
export function deactivate() {



}