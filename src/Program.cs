﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Azurite
{
    public class Parser
    {
        public class QuickException : Exception
        {
            public QuickException(string info) : base(info) { }
        }
        public class pair<FirstType, SecondType>
        {
            protected FirstType _first;
            protected SecondType _second;
            public FirstType first()
            {
                return _first;
            }
            public void first(FirstType value)
            {
                _first = value;
            }
            public SecondType second()
            {
                return _second;
            }
            public void second(SecondType value)
            {
                _second = value;
            }
        }

        public class SExpression : pair<SExpression, SExpression>
        {

            public string data { get; set; }
            public bool has_data { get; set; }
            public bool is_end { get; set; }
            /*public static string modify_parenthesis(string to_modify){
				
			}*/

            public static string insert_nulls(string data)
            {

                Regex reg = new Regex(@"\)(?=(?:[^""]*""[^""]*"")*[^""]*$)");
                return reg.Replace(data, " NULL )");
                // return data.Replace(")", " NULL )");
            }
            public static List<string> remove_parenthesis(List<string> data)
            {
                return (data[0] == "(" && data[data.Count - 1] == ")") ? data.GetRange(1, data.Count - 2) : data;
            }
            public static List<string> remove_first_and_last(List<string> data)
            {
                return data.GetRange(1, data.Count - 2);
            }

            public static List<string> trim(List<string> data)
            {
                List<string> to_return = new List<string>();
                foreach (var item in data)
                {
                    if (item != "")
                        to_return.Add(item.Trim());
                }
                return to_return;
            }

            public static void print_list(List<string> data)
            {
                foreach (var item in data)
                {
                    Console.Write("'{0}' ", item);
                }
                Console.WriteLine("");
            }

            public static List<string> tokenize(string data)
            {
                List<string> to_return = new List<string>();
                to_return.Add("");
                bool current_between_parenthesis = false;
                foreach (char c in data)
                {
                    if (c != '\n' && c != '\r' && c != ' ' && c != '"')
                    {
                        to_return[to_return.Count - 1] += c;
                    }
                    else if (c == ' ')
                    {
                        if (current_between_parenthesis)
                        {
                            to_return[to_return.Count - 1] += ' ';
                        }
                        else
                            to_return.Add("");
                    }
                    else if (c == '"')
                    {
                        to_return[to_return.Count - 1] += c;
                        current_between_parenthesis = !current_between_parenthesis;
                    }
                }
                return to_return;
            }

            public static bool has_parenthesis_inside(List<string> data)
            {
                foreach (var str in remove_first_and_last(data))
                {
                    if (str == ")")
                        return true;
                    if (str == ")") //"(" ???
                        return true;
                }
                return false;
            }

            public static List<string> sanitize_parenthesis(List<string> data)
            {
                var to_return = new List<string>(data);
                for (int i = 1; i < data.Count - 2; i++)
                {
                    if (data[i] == "(" && data[i + 1] == "(")
                    {
                        var y = find_matching_parenthesis(data, i + 1);
                        if (y == find_matching_parenthesis(data, i) - 1)
                        {
                            to_return.RemoveAt(i + 1);
                            to_return.RemoveAt(y);
                        }
                    }
                }
                return to_return;
            }

            public static int find_matching_parenthesis(List<string> data, int start)
            {
                int pos = 0;
                uint count = 1;
                foreach (var element in data)
                {
                    if (pos > start)
                    {
                        if (element == "(")
                        {
                            count++;
                        }
                        else if (element == ")")
                        {
                            count--;
                        }
                    }
                    if (count == 0)
                    {
                        return pos;
                    }
                    pos++;
                }
                return -1;
            }
            public static string add_spaces(string data){
                string to_return = "";
                bool p = false;
                foreach(var	c in data){
                    if(!p && (c == '(' || c == ')')){
                        to_return += $" {c} ";
                    } else if(c == '"'){
                        p = !p;
                        to_return += c;
                    } else{
                        to_return += c;
                    }
                }
                return to_return;
            }

            public SExpression()
            {
                data = "NULL";
                has_data = true;
                is_end = true;
            }
            public SExpression(List<string> array, uint depth = 0)
            {
                //Console.Write("{0} ] CALL WITH LIST : ", depth);
                //print_list(array);
                //Console.WriteLine("\n");
                if (array.Count == 1)
                {
                    //Console.WriteLine("checkpoint");
                    this.data = array[0];
                    has_data = true;
                }
                else
                {
                    int count = 0;
                    has_data = false;
                    array = trim(array);
                    var head = array[0].Trim();
                    if (head != "(" && head != ")")
                    {
                        //Console.WriteLine("checkpoint {0}", array.Count-1);
                        _first = new SExpression(head, depth + 1);
                        if (array[1] == "NULL")
                        {
                            _second = new SExpression();
                        }
                        else
                            _second = new SExpression(array.GetRange(1, array.Count - 1), depth + 1);
                    }
                    else if (head == "(")
                    {
                        var pos = find_matching_parenthesis(array, count);
                        _first = new SExpression(array.GetRange(1, pos - 1), depth + 1);
                        _second = new SExpression(array.GetRange(pos + 1, array.Count - pos - 1), depth + 1);
                    }
                    else
                    { //if(head == ")")
                        throw new QuickException(" mismatched ')' found");
                    }
                    count++;
                }
            }
            public SExpression(string param, uint depth = 0)
            {
                try{
                    //Console.WriteLine("{0} ]CALL WITH STRING : {1}", depth, param);
                    //param = param.Replace("(", " ( ");
                    //param = param.Replace(")", " ) ");
                    param = add_spaces(param);
                    List<string> array = new List<string>(tokenize(param)); //(param.Split(' '));
                    if (array[0] == "" || array[0] == " ")
                        array = remove_first_and_last(array);

                    array = remove_parenthesis(array);
                    array = trim(array);
                    array = sanitize_parenthesis(array);
                    //array = trim(array);
                    if (array.Count == 1)
                    {
                        this.data = array[0];
                        has_data = true;
                    }
                    else
                    {
                        int count = 0;
                        has_data = false;
                        var head = array[0].Trim();
                        if (head != "(" && head != ")")
                        {
                            //Console.WriteLine("checkpoint {0}", array.Count-1);
                            _first = new SExpression(head, depth + 1);
                            if (array[1] == "NULL")
                            {
                                _second = new SExpression();
                            }
                            else
                                _second = new SExpression(array.GetRange(1, array.Count - 1), depth + 1);
                            //_second = new SExpression(array.GetRange(1, array.Count-1), depth+1);
                        }
                        else if (head == "(")
                        {
                            var pos = find_matching_parenthesis(array, count);
                            //Console.WriteLine(pos);
                            _first = new SExpression(array.GetRange(1, pos - 1), depth + 1);
                            _second = new SExpression(array.GetRange(pos + 1, array.Count - pos - 1), depth + 1);
                        }
                        else
                        { //if(head == ")")
                            throw new QuickException(" mismatched ')' found");
                        }
                        count++;
                    }
                } catch(Exception e){
                    throw new Exception("Error while parsing " + param);
                }
            }

            public SExpression(List<string> array)
            {
                //Console.Write("CALL WITH LIST : ");
                //print_list(array);
                //Console.WriteLine("\n");
                if (array.Count == 1)
                {
                    this.data = array[0];
                    has_data = true;
                }
                else
                {
                    int count = 0;
                    has_data = false;
                    array = trim(array);
                    var head = array[0].Trim();
                    if (head != "(" && head != ")")
                    {
                        //Console.WriteLine("checkpoint {0}", array.Count-1);
                        _first = new SExpression(head);
                        if (array[1] == "NULL")
                        {
                            _second = new SExpression();
                        }
                        else
                            _second = new SExpression(array.GetRange(1, array.Count - 1));
                        //_second = new SExpression(array.GetRange(1, array.Count-1));
                    }
                    else if (head == "(")
                    {
                        var pos = find_matching_parenthesis(array, count);
                        _first = new SExpression(array.GetRange(1, pos - 1));
                        _second = new SExpression(array.GetRange(pos + 1, array.Count - pos - 1));
                    }
                    else
                    { //if(head == ")")
                        throw new QuickException(" mismatched ')' found");
                    }
                    count++;
                }
            }
            public SExpression(string param)
            {
                try{
                    param = add_spaces(param);
                    param = param.Trim();
                    param = insert_nulls(param);
                    //Console.WriteLine("CALL WITH STRING : {0}", param);
                    //param = param.Replace("(", " ( ");
                    //param = param.Replace(")", " ) ");
                    List<string> array = new List<string>(tokenize(param)); //(param.Split(' '));
                    if (array[0] == "" || array[0] == " ")
                        array = remove_first_and_last(array);

                    array = remove_parenthesis(array);
                    array = sanitize_parenthesis(array);
                    array = trim(array);
                    //array = trim(array);
                    if (array.Count == 1)
                    {
                        this.data = array[0];
                        has_data = true;
                    }
                    else
                    {
                        int count = 0;
                        has_data = false;
                        var head = array[0].Trim();
                        if (head != "(" && head != ")")
                        {
                            //Console.WriteLine("checkpoint {0}", array.Count-1);
                            _first = new SExpression(head);
                            if (array[1] == "NULL")
                            {
                                _second = new SExpression();
                            }
                            else
                                _second = new SExpression(array.GetRange(1, array.Count - 1));
                            //_second = new SExpression(array.GetRange(1, array.Count-1));
                        }
                        else if (head == "(")
                        {
                            var pos = find_matching_parenthesis(array, count);
                            if (pos == array.Count - 1)
                            {

                            }
                            else
                            {
                                _first = new SExpression(array.GetRange(1, pos - 1));
                                _second = new SExpression(array.GetRange(pos + 1, array.Count - pos - 1));
                            }
                        }
                        else
                        { //if(head == ")")
                            throw new QuickException(" mismatched ')' found");
                        }
                        count++;
                    }
                } catch(Exception e){
                    throw new Exception("Error while parsing " + param);
                }
            }

            public SExpression(SExpression sExpression)
            {
                //Take an SExpression as entries copy the data
                this.data = sExpression.data;
                this.has_data = sExpression.has_data;
                this.is_end = sExpression.is_end;

                //Clone the first and right child
                if (sExpression._first != null)
                    this._first = sExpression._first.Clone();
                if (sExpression._second != null)
                    this._second = sExpression._second.Clone();
            }

            public SExpression Clone()
            {
                return new SExpression(this);
            }

            public SExpression(List<SExpression> liste)
            {
                SExpression current = this;
                for (int i = 0; i < liste.Count; i++)
                {
                    current.data = null;
                    current._first = liste[i];
                    current._second = new Parser.SExpression();
                    current = current.second();
                }
            }

            public void print(uint level = 0)
            {
                if (this.has_data)
                {
                    for (uint i = 0; i < level; i++)
                    {
                        Console.Write("-");
                    }
                    Console.Write("{0}\n", data);
                    //Console.WriteLine();
                }
                else
                {
                    _first.print(level + 1);
                    _second.print(level + 1);
                }
            }

            //source: https://stackoverflow.com/questions/1649027/how-do-i-print-out-a-tree-structure
            public void PrettyPrint(string indent = "")
            {

                bool last = _first == null && _second == null;
                Console.WriteLine(indent + "+- " + data);
                indent += last ? "   " : "|  ";

                if (_first != null)
                    _first.PrettyPrint(indent);
                if (_second != null)
                    _second.PrettyPrint(indent);

            }
            public int getDeph()
            {
                if (this.data == "NULL")
                    return 0;

                //* Source: https://www.educative.io/edpresso/finding-the-maximum-depth-of-a-binary-tree
                int leftDepth = _first.getDeph();
                int rightDepth = _second.getDeph();

                // Get the larger depth and add 1 to it to
                // account for the root.
                if (leftDepth > rightDepth)
                    return (leftDepth + 1);
                else
                    return (rightDepth + 1);
            }

            public bool Equal(Parser.SExpression test_expression) =>
            this.has_data == test_expression.has_data &&
            this.is_end == test_expression.is_end &&
            this.data == test_expression.data &&
            (this._first != null) ? this._first.Equal(test_expression.first()) : true &&
                (this._second != null) ? this._second.Equal(test_expression.second()) : true;
            // public static bool operator != (Parser.SExpression test, Parser.SExpression test_expression) => !(test == test_expression);

            public List<string> LoadAllData()
            {
                List<string> parameters = new List<string>();

                Parser.SExpression current = this;
                if (current.first() == null)
                {
                    parameters.Add(current.data);
                    return parameters;

                }
                while (current.data != "NULL")
                {
                    parameters.Add(current.first().data);
                    current = current.second();
                }

                return parameters;
            }

            public string Stringify(string start, string end)
            {
                return start + this.data +
                    ((this.first() != null) ? this.first().Stringify(start, end) : "") +
                    ((this.second() != null) ? this.second().Stringify(start, end) : "") +
                    end;
            }
            /// <summary>
            /// Load all sub S-expression contains in the current S-expression.
            /// </summary>
            /// <returns>Return a list of S-expression</returns>
            ///<example>
            /// Admitting this S-expression: ((+ 2 3) (foo)) this method will return (+ 2 3),(foo) inside a list.
            /// </example>
            public List<Parser.SExpression> LoadAllChild()
            {
                List<Parser.SExpression> parameters = new List<SExpression>();

                Parser.SExpression current = this;
                while (current.data != "NULL")
                {

                    parameters.Add(current.first());
                    current = current.second();
                }

                return parameters;
            }

            public bool Match(Parser.SExpression pattern) =>
            this.data == pattern.data &&
            ((pattern.first() != null) && this.first() != null) ? this.first().Match(pattern.first()) : true &&
                ((pattern.second() != null) && this.second() != null) ? this.second().Match(pattern.second()) : true;

            private void ImportExpression(SExpression expression)
            {
                this._first = expression.first();
                this._second = expression.second();
                this.data = expression.data;
                this.has_data = expression.has_data;
                this.is_end = expression.is_end;
            }

            /// <summary>
            /// Apply a function to every node of the S-expression
            /// </summary>
            /// <param name="function">The fonction to apply</param>
            public void Map(Func<SExpression, SExpression> function)
            {
                SExpression newExpression = function(this);
                ImportExpression(newExpression);
                if (this._first != null)
                    this._first.Map(function);
                if (this._second != null)
                    this._second.Map(function);
            }

            // public static implicit operator Dictionary<object, object>(SExpression v)
            // {
            //     throw new NotImplementedException();
            // }

        }

    }
    class MainClass
    {
        public const int MAX_RECURSION_ALLOWED = 100;
        public static void Main(string[] args)
        {

            Lexer.init_builtins();

            if (args.Length > 0)
            {


                // string filePath = "";
                ParameterManagers.OnInput = input => {if(!Langconfig.is_loaded) Langconfig.load(); Azurite.Load(input);};
                List<string> languages = new List<string>();

                ParameterManagers.registerCommand(
                    new ParameterManagers.Command("-t", langs => languages = langs,
                        new List<string>() { "--target" },
                        true
                        ));



                ParameterManagers.registerCommand(
                    new ParameterManagers.Command("-s", output =>
                    {
                        foreach (string lang in languages)
                        {
                            string file_extension = Azurite.GetFileExtension(lang);
                            Azurite.Export(output[0] + "." + file_extension, lang);
                            Console.WriteLine($"file save at: {Path.GetFullPath(output[0] + "." + file_extension)}");
                        }
                    },
                    new List<string>() { "--save" },
                    true
                    ));

                ParameterManagers.registerCommand(
                    new ParameterManagers.Command("-l", output =>
                    {
                        Console.Write(Azurite.LanguageHandler.LogLanguage());
                    },
                        new List<string>() { "--list" },
                        true
                        ));

                ParameterManagers.registerCommand(
                    new ParameterManagers.Command("-c",
                        output =>
                        {
                            Langconfig.load(output[0]);

                        },
                        new List<string>() { "--config" },
                        false,
                        () => { Langconfig.load(); }
                    ));



                ParameterManagers.Execute(args);

            }
            else
            {
                Langconfig.load();

                new REPL();
            }
        }
    }
}