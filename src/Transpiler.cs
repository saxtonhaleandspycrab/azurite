namespace Azurite
{
    /// <summary>
    /// Transpiler contain the mains method to convert an S-expression.
    /// </summary>
    public class Transpiler
    {
        /// <summary>
        /// Convert an S-expression in the specified language
        /// </summary>
        /// <param name="expression">The expression to convert.</param>
        /// <param name="language">The language to convert in.</param>
        /// <returns>Return the expression converted in the language.</returns>
        public static string Convert(Parser.SExpression expression, string language)
        {
            //If no child then return the data fo the current expression if there is data.
            if (expression.first() == null)
                return (expression.data == "NULL") ? "" : expression.data;
            if (expression.first().data == Langconfig.function_name)
                Formal.type_of_func(expression);


            // Try to find some translate in the expression and convert them into string.
            string test = Directive.Execute(language, expression);
            if (test != "")
                return test;

            // If the first child has data

            //Get the data of the first child then convert the second.
            if (!expression.second().is_end)
            {
                // return "not found";
                throw new System.Exception("no translate found");

            }
            return Convert(expression.first(), language);

        }
        public static string Convert(string input, string language)
        {
            return Transpiler.Convert(new Parser.SExpression(input), language);
        }
    }

}