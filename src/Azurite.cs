using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Azurite
{
    /// <summary>
    /// Main azurite file containing the method for loading file and converting.
    /// </summary>
    public static class Azurite
    {

        /// <summary>
        /// Language currently imported inside Azurite.
        /// </summary>
        public static class LanguageHandler
        {

            private static Dictionary<string, int> lang = new Dictionary<string, int>();

            /// <summary>
            /// Add a language to Azurite.
            /// </summary>
            /// <param name="language">The language to add.</param>
            /// <returns>True if the language wasn't yet present.</returns>
            public static bool AddLanguage(string language)
            {
                if (!lang.ContainsKey(language))
                {
                    lang.Add(language, lang.Count);
                    return true;
                }
                return false;
            }

            /// <summary>
            /// Get the unique id of the language depending of the position at which the language has been added.
            /// </summary>
            /// <param name="language">The language to add.</param>
            /// <returns>Return the id of the language if found otherwise -1.</returns>
            public static int getLanguageIndex(string language)
            {
                if (lang.ContainsKey(language))
                    return lang[language];
                throw new ArgumentException($"can't find language {language}. Language available:{String.Join(',', new List<string>(lang.Keys))}");
            }

            /// <summary>
            /// Get the list of languages separated with ','.
            /// </summary>
            /// <returns>Return the list of languages separated with ','.</returns>
            public static string LogLanguage()
            {
                return String.Join(',', new List<string>(lang.Keys));
            }
        }

        /// <summary> 
        /// An expression is just the parsed line associated with the value of the line
        /// </summary>
        public struct Expression
        {
            public Parser.SExpression arbre;
            public string line;
            public Expression(Parser.SExpression arbre, string line)
            {
                this.arbre = arbre;
                this.line = line;
            }
        }


        static List<Expression> expressions_list = new List<Expression>();

        /// <summary>
        /// Reset Azurite.
        /// </summary>
        public static void Reset()
        {
            List<Parser.SExpression> expressions_list = new List<Parser.SExpression>();
        }

        /// <summary> Load a file in the current azurite runtime
        /// <param name="path"> the path of the file to load </param>
        /// <param name="filename"> if specified the tag to add to every method and function of the file </param>
        /// </summary>
        public static void Load(string path, string filename = "")
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("Cannot found: " + path);

            List<string> output = new List<string>();

            string[] fileContent = File.ReadAllLines(path);
            for (int index = 0; index < fileContent.Length;)
            {
                string newLine = "";
                do
                {
                    string line = fileContent[index].Replace("(", " ( ").Replace(")", " ) ");
                    newLine += line;

                    index++;
                } while (Parser.SExpression.find_matching_parenthesis(
                        Parser.SExpression.trim(
                            Parser.SExpression.tokenize(newLine)), 0) == -1 && index < fileContent.Length);

                output.Add(newLine);
            }

            List<string> convert = new List<string>();

            for (int index = 0; index < output.Count; index++)
            {
                string line = output[index];

                if (line == "")
                    continue;

                try
                {
                    Parser.SExpression expression = new Parser.SExpression(line);
                    Process(new Expression(expression, line), filename);
                }
                catch (System.Exception e)
                {
                    throw new ArgumentException($"{path} at {output[index]} at approx line {index + 1}", e);
                }
            }

            Console.WriteLine("successfully loaded: " + path + " in the current Azurite runtime");
        }

        /// <summary> Check if <paramref name="expression"/> start with an explicit token and do the appropriate action
        /// <param name="expression">The expression to check.</param>
        /// <param name="filename">If specified the tag to the expression if needed</param>
        /// </summary>
        public static void Process(Expression expression, string filename = "")
        {
            Parser.SExpression arbre = expression.arbre;

            if (arbre.has_data && arbre.data == "NULL")
                return;

            if (arbre.first().data == Langconfig.macro_name)
                MacroManager.LoadMacro(arbre, filename);

            else if (arbre.first().data == Langconfig.translate_name)
                Directive.LoadInstruction(arbre.second(), filename);

            else if (arbre.first().data == Langconfig.function_name)
            {
                Directive.known_token.Add(arbre.second().first().data);
                arbre = MacroApply(arbre);
                Formal.type_of_func(arbre);
                expression.arbre = MacroApply(arbre);
                expressions_list.Add(expression);
            }
            else if (arbre.first().data == Langconfig.import_name)
            {
                List<string> data = arbre.LoadAllData();
                Load(data[1].Replace("\"", "").Replace($"{{{Langconfig.libpath}}}", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).Replace("\\", "/") + "/.azurite"), (data.Count > 2) ? data[2] : "");
            }
            else
            {
                expression.arbre = MacroApply(expression.arbre);
                if(!expression.arbre.has_data && expression.arbre.data != "NULL")
                    Formal.descendent_verification(expression.arbre);
                expressions_list.Add(expression);
            }
        }

        /// <summary> Apply macro in the specified expression
        /// <param name="expression">The expression to check in.</param>
        /// <return>The expression without macro inside.</return>
        /// </summary>
        public static Parser.SExpression MacroApply(Parser.SExpression expression)
        {
            expression = MacroManager.Execute(expression);
            // if (expression.first() != null)
            // {
            //     // if (MacroManager.CheckMacro(expression.first().data))
            //     //     return MacroManager.FindMacro(expression);

            //     // If there is nothing to do on this expression keep looping throught the trees.
            //     expression.first(MacroApply(expression.first()));
            // }

            // if (expression.second() != null)
            // expression.second(MacroApply(expression.second()));

            return expression;
        }


        /// <summary> Translate the expression list in a specified language.
        /// <param name="language">The targeted language.</param>
        /// <return>A list containing the translated list.</return>
        /// </summary>
        public static List<string> ExpressionToString(string language)
        {
            List<string> expressions = new List<string>();
            int line = 0;

            //try
            //{
                for (; line < expressions_list.Count; ++line)
                {
                    Parser.SExpression expression = expressions_list[line].arbre;
                    string text = Transpiler.Convert(expression, language);

                    if (text != "")
                        expressions.Add(text);
                }
            //}
            //catch (System.Exception e)
            //{
            //    throw new System.Exception($"at {expressions_list[line].line}", e);
            //}

            return expressions;
        }

        /// <summary>
        /// Take a a string <paramref name="input"/> and convert it in <paramref name="language"/>.
        /// </summary>
        /// <param name="input">The string to convert.</param>
        /// <param name="language">The language to convert in.</param>
        /// <returns></returns>
        public static string TranslateExpression(string input, string language)
        {
            Parser.SExpression expression = new Parser.SExpression(input);
            return TranslateExpression(expression, language, input);
        }

        /// <summary>
        /// Take an S-expression and convert it.
        /// </summary>
        /// <param name="expression">The S-expression to convert.</param>
        /// <param name="language">The targeted language for the conversion.</param>
        /// <param name="input">The non parsed expression.</param>
        /// <returns>Return the expression converted in the specified language.</returns>
        public static string TranslateExpression(Parser.SExpression expression, string language, string input = "")
        {
            Azurite.Process(new Expression(expression, input));
            return Transpiler.Convert(expression, language);
        }

        /// <summary>
        /// Save and translate the S-expression loaded in Azurite.cs
        /// </summary>
        /// <param name="path">The adress of the output file.</param>
        /// <param name="language">The language in wich the S-expression must be converted.</param>
        public static void Export(string path, string language)
        {
            List<string> imports = Directive.Imports_list;
            List<string> conversion = new List<string>(imports.Concat(ExpressionToString(language)));

            for (int i = 0; i < conversion.Count; i++)
            {
                conversion[i] = conversion[i].Replace("\\n", "\n").Replace("\\t", "\t").Replace("\\\"", "\"");
            }
            Directive.Imports_list = new List<string>();

            File.WriteAllLines(path, conversion);

        }


        /// <summary>
        /// Print all expression stored in Azurite.
        /// </summary>
        public static void DisplayExpressions()
        {
            foreach (Expression expression in expressions_list)
            {
                expression.arbre.PrettyPrint();
            }
        }

        /// <summary>
        /// Get the file extension for a language.
        /// </summary>
        /// <param name="language">The language to get the extension from.</param>
        /// <returns>The extension of the language.</returns>
        public static string GetFileExtension(string language)
        {
            return Transpiler.Convert(new Parser.SExpression($"({language})"), language);
        }
    }
}