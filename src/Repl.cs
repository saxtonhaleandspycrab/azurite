using System;
using System.Collections.Generic;

namespace Azurite{
    class REPL{

        public static string get_extension(string lang){
            lang = lang.ToLower().Trim();
            if(lang == "c++" || lang == "cpp" || lang == "cplusplus" || lang == "cxx") return "cpp";
            if(lang == "python" || lang == "python3" || lang == "py" || lang == "py3") return "py";
            if(lang == "caml" || lang == "ocaml" || lang == "ml") return "ml";
            if(lang == "js" || lang == "javascript") return "js";
            if(lang == "cs" || lang == "csharp" || lang == "c#") return "cs";
            if(lang == "azurite" || lang == "azur") return "azur";
            return "error";
        }

        // public static string get_lang_id(string lang_extension){
        //     lang_extension = get_extension(lang_extension);
        //     switch(lang_extension){
        //         case "cpp":
        //             return Azurite.@string.cpp;
        //         case "py":
        //             return Azurite.@string.python;
        //         case "cs":
        //             return Azurite.@string.csharp;
        //         case "ml":
        //             return Azurite.@string.ocaml;
        //         case "js":
        //             return Azurite.@string.javascript;
        //         default:
        //             throw new NotImplementedException("get_lang_id");
        //     }
        // }

        public string get_lang_name(string lang_extension){
            lang_extension = get_extension(lang_extension);
            switch(lang_extension){
                case "cpp":
                    return "C++";
                case "py":
                    return "Python 3";
                case "cs":
                    return "C#";
                case "ml":
                    return "OCaml";
                case "js":
                    return "Javascript";
                case "azur":
                    return "Azurite";
                default:
                    throw new NotImplementedException($"get_lang_name {lang_extension}");
            }
        }

        public void print_head(string lang_str){
            Console.WriteLine($"Azurite REPL started, with language {get_lang_name(get_extension(lang_str))}.");
            //Console.WriteLine("Use function type to get the type of an atom.");
        }

        bool debug_mode(){
            return false;
        }

        //Laisse l'output transpilé dans stdout
        public REPL(string language_name){
            print_head(language_name);
            language_name = language_name.ToLower().Trim();
            if(language_name == "azurite" || language_name == "azur" ){
                do{
                    string s;
                    var ast = loop_iter(out s).Clone();
                    ast = MacroManager.Execute(ast);
                    if(ast.has_data && ast.data.ToLower().Trim() == "quit"){
                        return;
                    }
                    if(!debug_mode()){
                        try{
                            if(!ast.has_data && ast.first().has_data && ast.first().data == Langconfig.function_name){
                                Console.WriteLine(Tools.get_pretty_type(Formal.type_of_func(ast)));
                            } else if(!ast.has_data && ast.first().has_data && (ast.first().data == Langconfig.macro_name 
                                                                            ||  ast.first().data == Langconfig.translate_name
                                                                            ||  ast.first().data == Langconfig.import_name)){
                                Azurite.Process(new Azurite.Expression(new Parser.SExpression(ast), ast.Stringify("(", ")")));
                            } else{
                                if(!ast.has_data)
                                    Formal.descendent_verification(ast, "#1");
                                Console.WriteLine(Tools.get_pretty_type(Formal.type_of(ast)));
                            }
                        } catch(Exception e){
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(e.Message);
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    } else{
                        if(!ast.has_data && ast.first().has_data && ast.first().data == Langconfig.function_name){
                            Console.WriteLine(Tools.get_pretty_type(Formal.type_of_func(ast)));
                        } else if(!ast.has_data && ast.first().has_data && (ast.first().data == Langconfig.macro_name 
                                                                        ||  ast.first().data == Langconfig.translate_name
                                                                        ||  ast.first().data == Langconfig.import_name)){
                            Azurite.Process(new Azurite.Expression(new Parser.SExpression(ast), ast.Stringify("(", ")")));
                        }else{
                            if(!ast.has_data)
                                Formal.descendent_verification(ast, "#1");
                            Console.WriteLine(Tools.get_pretty_type(Formal.type_of(ast)));
                        }
                    }
                }while(true);
            } else{
                do{
                    string s;
                    var ast = loop_iter(out s).Clone();
                    ast = MacroManager.Execute(ast);
                    if(ast.has_data && ast.data.ToLower().Trim() == "quit"){
                        return;
                    }
                    if(!debug_mode()){
                        try{
                            if(!ast.has_data && ast.first().has_data && ast.first().data == Langconfig.function_name){
                                Console.WriteLine(Tools.get_pretty_type(Formal.type_of_func(ast)));
                            } else if(!ast.has_data && ast.first().has_data && (ast.first().data == Langconfig.macro_name 
                                                                            ||  ast.first().data == Langconfig.translate_name
                                                                            ||  ast.first().data == Langconfig.import_name)){
                                Azurite.Process(new Azurite.Expression(new Parser.SExpression(ast), ast.Stringify("(", ")")));
                            }else{
                                if(!ast.has_data)
                                    Formal.descendent_verification(ast, "#1");
                                Console.WriteLine(Tools.get_pretty_type(Formal.type_of(ast)));
                            }
                        } catch(Exception e){
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(e.Message);
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    } else{
                        if(!ast.has_data && ast.first().has_data && ast.first().data == Langconfig.function_name){
                            Console.WriteLine(Tools.get_pretty_type(Formal.type_of_func(ast)));
                        } else if(!ast.has_data && ast.first().has_data && (ast.first().data == Langconfig.macro_name 
                                                                        ||  ast.first().data == Langconfig.translate_name
                                                                        ||  ast.first().data == Langconfig.import_name)){
                            Azurite.Process(new Azurite.Expression(new Parser.SExpression(ast), ast.Stringify("(", ")")));
                        } else{
                            if(!ast.has_data)
                                Formal.descendent_verification(ast, "#1");
                            Console.WriteLine(Tools.get_pretty_type(Formal.type_of(ast)));
                        }
                    }
                    Console.WriteLine(Azurite.TranslateExpression(ast, language_name) + "\n");         ///apply read_and_export
                }while(true);
            }
        }

        //Écrit l'output transpilé dans un fichier
        public REPL(string language_name, string output){
            print_head(language_name);
            language_name = language_name.ToLower().Trim();
            if(language_name == "azurite" || language_name == "azur" ){
                do{
                    string s;
                    var ast = loop_iter(out s).Clone();
                    ast = MacroManager.Execute(ast);
                    if(ast.has_data && ast.data.ToLower().Trim() == "quit"){
                        return;
                    }
                    if(!debug_mode()){
                        try{
                            if(!ast.has_data && ast.first().has_data && ast.first().data == Langconfig.function_name){
                                Console.WriteLine(Tools.get_pretty_type(Formal.type_of_func(ast)));
                                System.IO.File.WriteAllText(output + "." + get_extension(language_name), s + "\n");
                            } else{
                                if(!ast.has_data)
                                    Formal.descendent_verification(ast);
                                Console.WriteLine(Tools.get_pretty_type(Formal.type_of(ast)));
                            }
                        } catch(Exception e){
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(e.Message);
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }else{
                        if(!ast.has_data && ast.first().has_data && ast.first().data == Langconfig.function_name){
                            Console.WriteLine(Tools.get_pretty_type(Formal.type_of_func(ast)));
                            System.IO.File.WriteAllText(output + "." + get_extension(language_name), s + "\n");
                        } else{
                            if(!ast.has_data)
                                Formal.descendent_verification(ast);
                            Console.WriteLine(Tools.get_pretty_type(Formal.type_of(ast)));
                        }
                    }
                    
                }while(true);
            } else{
                do{
                    string s;
                    var ast = loop_iter(out s).Clone();
                    if(ast.has_data && ast.data.ToLower().Trim() == "quit"){
                        return;
                    }
                    System.IO.File.WriteAllText(output + "." + get_extension(language_name), Azurite.TranslateExpression(ast, language_name) + "\n");         ///apply read_and_export
                }while(true);
            }
        }
        public Parser.SExpression loop_iter(out string cumul){
            cumul = "";
            bool has_passed = false;
            do{
                Console.Write(has_passed?"... ":">>> ");
                cumul += Console.ReadLine();
                try{
                    var r = new Parser.SExpression(cumul);
                    cumul = cumul.Trim();
                    return r;
                } catch(System.Exception){
                    has_passed = true;
                    continue;
                }
                // has_passed = true;
            }while(true);
            throw new Exception("WTF!!!!!!!!!!!!!!!!!!!!!!!!");
        }

        public string loop_iter_to_s(){
            bool has_passed = false;
            do{
                Console.Write(has_passed?"... ":">>> ");
                string cumul = "";
                cumul += Console.ReadLine();
                try{
                    var r = new Parser.SExpression(cumul);
                    return cumul;
                } catch(System.Exception){
                    has_passed = true;
                    continue;
                }
                // has_passed = true;
            }while(true);
            throw new Exception("WTF!!!!!!!!!!!!!!!!!!!!!!!!");
        }
        
        public REPL(){
            new REPL("azurite");
        }
    }
}