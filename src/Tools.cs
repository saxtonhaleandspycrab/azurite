using System;
using System.Collections.Generic;

namespace Azurite
{
    //\brief static tools functions
    class Tools
    {
        public static bool list_equal<T>(List<T> a, List<T> b) where T : class
        {
            if (a.Count != b.Count) return false;
            for (int i = 0; i < a.Count; i++)
            {
                if (a[i] != b[i])
                {
                    return false;
                }
            }
            return true;
        }
        public static string get_pretty_type(List<string> type)
        {
            string to_return = "";
            for (int i = 0; i < type.Count - 1; i++)
            {
                to_return += type[i] + " -> ";
            }
            to_return += type[type.Count - 1];
            return to_return;
        }

        public static string get_from_ast(Parser.SExpression expr)
        {
            // string to_return = "";
            if (expr.has_data) return expr.data;
            return "(" + get_from_ast(expr.first()) + " " + get_from_ast(expr.second()) + ")";
        }

        public static string repeat_string(string to_repeat, string bind, uint occurences)
        {
            if (occurences == 0) return "";
            string r = "";
            while (occurences > 1)
            {
                r = r + to_repeat + bind;
                occurences--;
            }
            return r + to_repeat;
        }

        public static List<T> tail<T>(List<T> to_get)
        {
            if (to_get.Count < 2) return new List<T>();
            return to_get.GetRange(1, to_get.Count - 1);
        }

        private static bool is_of_match_level(Parser.SExpression expression, Directive.MATCH_LEVEL lvl, string data_token = "")
        {
            switch (lvl)
            {
                //case Directive.MATCH_LEVEL.CALLABLE:
                //    return Formal.type_of(expression).Count > 1;
                case Directive.MATCH_LEVEL.EXACT:
                    return expression.has_data && expression.data == data_token.Trim('"');
                case Directive.MATCH_LEVEL.STRICT:
                    return expression.has_data;
                case Directive.MATCH_LEVEL.LIGHT:
                    return true;
                case Directive.MATCH_LEVEL.LIST:
                    return !expression.has_data;
                default:
                    throw new NotImplementedException("Match value added, not used here.");
            }
            throw new NotImplementedException("Match value added, not used here.");
        }

        private static Directive.MATCH_LEVEL get_match_level_of(string data)
        {
            if (data.StartsWith("\"") && data.EndsWith("\""))
                return Directive.MATCH_LEVEL.EXACT;
            if (data.EndsWith("..."))
                return Directive.MATCH_LEVEL.LIST;
            if (data.StartsWith("'") && data.EndsWith("'"))
                return Directive.MATCH_LEVEL.STRICT;
            //if(data.StartsWith("[") && data.EndsWith("]"))
            //    return Directive.MATCH_LEVEL.CALLABLE;
            return Directive.MATCH_LEVEL.LIGHT;
        }

        private static string get_token(string data, Directive.MATCH_LEVEL curlvl)
        {
            switch (curlvl)
            {
                case Directive.MATCH_LEVEL.EXACT:
                    return data.Trim('\"');
                case Directive.MATCH_LEVEL.STRICT:
                    return data.Trim('\'');
                case Directive.MATCH_LEVEL.LIST:
                    return data.Remove(data.Length - 3);
                case Directive.MATCH_LEVEL.LIGHT:
                    return data;
                default:
                    throw new NotImplementedException("Match value added, not used here.");
            }
            throw new NotImplementedException("Match value added, not used here.");
        }

        private static Dictionary<string, Parser.SExpression> Merge(Dictionary<string, Parser.SExpression> a, Dictionary<string, Parser.SExpression> b){
            var to_return = new Dictionary<string, Parser.SExpression>();
            if(b.Count == 0)
                return a;
            if(a.Count == 0)
                return b;

            foreach (var scrut in b){
                if (a.ContainsKey(scrut.Key) && get_from_ast(a[scrut.Key]) != get_from_ast(scrut.Value)){
                    throw new Exception($"Macro incompatibility in subcall.");
                }
                to_return.Add(scrut.Key, scrut.Value);
            }
            foreach (var scrut in a){
                if (!to_return.ContainsKey(scrut.Key))
                    to_return.Add(scrut.Key, scrut.Value);
            }
            return to_return;
        }

        /*private static Parser.SExpression fromChildren(List<Parser.SExpression> to_merge){
            if(to_merge.Count == 0)
                return new Parser.SExpression("()");
            if(to_merge.Count == 1)
                return to_merge[0];
            var to_return = new Parser.SExpression();
            to_return.first(to_merge[0].Clone());
            to_return.second(fromChildren(to_merge.GetRange(1, to_merge.Count-1)));
            return to_return;
        }*/

        ///<summary> Matches two SExpression in order to detect macro application</summary>
        ///<param name="reference"> The body of the macro prototype</param>
        ///<param name="to_match"> The SExpression to try to match</param>
        ///<returns> A Dictionary matching atoms in prototype with SExpression. Throws if unmached</returns> 
        public static Dictionary<string, Parser.SExpression> Match(Parser.SExpression reference, Parser.SExpression to_match){
            /*if ((reference.has_data && !to_match.has_data && get_match_level_of(reference.data) != Directive.MATCH_LEVEL.LIGHT) ||
                reference.has_data && reference.data == "NULL" && (!to_match.has_data || to_match.data != "NULL"))
                throw new Exception($"Macro incompatibility : {get_from_ast(reference)} / {get_from_ast(to_match)}");


            if (reference.has_data && is_of_match_level(to_match, get_match_level_of(reference.data), reference.data)){
                var lvl = get_match_level_of(reference.data);
                var to_return = new Dictionary<string, Parser.SExpression>();
                to_return.Add(get_token(reference.data, lvl), to_match);
                return to_return;
            }
            else if (reference.has_data){
                throw new Exception($"Macro incompatibility : {get_from_ast(reference)} / {get_from_ast(to_match)}");
            }
            else{
                var to_return = new Dictionary<string, Parser.SExpression>();
                var leftr = Match(reference.first(), to_match.first());
                if(reference.second().has_data  //|| to_match.second().has_data)
                    return leftr;

                var rightr = Match(reference.second(), to_match.second());

                foreach (var scrut in rightr){
                    if (leftr.ContainsKey(scrut.Key) && get_from_ast(leftr[scrut.Key]) != get_from_ast(scrut.Value)){
                        throw new Exception($"Macro incompatibility : {get_from_ast(reference)} / {get_from_ast(to_match)}");
                    }
                    to_return.Add(scrut.Key, scrut.Value);
                }
                foreach (var scrut in leftr){
                    if (!to_return.ContainsKey(scrut.Key))
                        to_return.Add(scrut.Key, scrut.Value);
                }
                return to_return;
            }*/
            var to_return = new Dictionary<string, Parser.SExpression>();
            int i = 0;
            var to_inspect = to_match.LoadAllChild();
            var proto = reference.LoadAllChild();
            foreach(var scrut in proto){
                if(scrut.has_data){
                    var level = get_match_level_of(scrut.data);
                    if(is_of_match_level(to_inspect[i], level, scrut.data)){
                        to_return = Merge(to_return, new Dictionary<string, Parser.SExpression>(){{get_token(scrut.data, level), to_inspect[i]}});
                    } else{
                        //check of last_index
                        if(i == proto.Count-1){ //add checks for types
                            to_return = Merge(to_return, new Dictionary<string, Parser.SExpression>()
                            {{get_token(scrut.data, level), new Parser.SExpression(to_inspect.GetRange(i, to_inspect.Count-i))}});
                        } else{
                            throw new Exception($"Macro incompatibility : {get_from_ast(reference)} / {get_from_ast(to_match)}");
                        }
                    }
                } else{ //launches recursively
                    to_return = Merge(to_return, Match(scrut, to_inspect[i]));
                }
                i++;
            }
            return to_return;

        }
    }
}