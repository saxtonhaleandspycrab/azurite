﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.install_azurite = New System.Windows.Forms.CheckBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.install_extension = New System.Windows.Forms.CheckBox()
        Me.uninstall_azurite = New System.Windows.Forms.CheckBox()
        Me.uninstall_extension = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(45, 109)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Installer"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'install_azurite
        '
        Me.install_azurite.AutoSize = True
        Me.install_azurite.Location = New System.Drawing.Point(45, 43)
        Me.install_azurite.Name = "install_azurite"
        Me.install_azurite.Size = New System.Drawing.Size(58, 17)
        Me.install_azurite.TabIndex = 1
        Me.install_azurite.Text = "Azurite"
        Me.install_azurite.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(194, 109)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Désinstaller"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'install_extension
        '
        Me.install_extension.AutoSize = True
        Me.install_extension.Location = New System.Drawing.Point(45, 67)
        Me.install_extension.Name = "install_extension"
        Me.install_extension.Size = New System.Drawing.Size(110, 17)
        Me.install_extension.TabIndex = 3
        Me.install_extension.Text = "Extension vscode"
        Me.install_extension.UseVisualStyleBackColor = True
        '
        'uninstall_azurite
        '
        Me.uninstall_azurite.AutoSize = True
        Me.uninstall_azurite.Location = New System.Drawing.Point(194, 43)
        Me.uninstall_azurite.Name = "uninstall_azurite"
        Me.uninstall_azurite.Size = New System.Drawing.Size(58, 17)
        Me.uninstall_azurite.TabIndex = 1
        Me.uninstall_azurite.Text = "Azurite"
        Me.uninstall_azurite.UseVisualStyleBackColor = True
        '
        'uninstall_extension
        '
        Me.uninstall_extension.AutoSize = True
        Me.uninstall_extension.Location = New System.Drawing.Point(194, 67)
        Me.uninstall_extension.Name = "uninstall_extension"
        Me.uninstall_extension.Size = New System.Drawing.Size(110, 17)
        Me.uninstall_extension.TabIndex = 3
        Me.uninstall_extension.Text = "Extension vscode"
        Me.uninstall_extension.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(350, 186)
        Me.Controls.Add(Me.uninstall_extension)
        Me.Controls.Add(Me.install_extension)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.uninstall_azurite)
        Me.Controls.Add(Me.install_azurite)
        Me.Controls.Add(Me.Button1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "Azurite installer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents install_azurite As CheckBox
    Friend WithEvents Button2 As Button
    Friend WithEvents install_extension As CheckBox
    Friend WithEvents uninstall_azurite As CheckBox
    Friend WithEvents uninstall_extension As CheckBox
End Class
