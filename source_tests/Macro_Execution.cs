using Azurite;
using Xunit;

namespace SourceTests
{
    public class Macro_Execution
    {
        // private readonly PrimeService _primeService;

        private Macro_manager _manager;

        public Macro_Execution()
        {

            string entries = "(macro (multi x y) (* x y))";
            Parser.SExpression expression = new Parser.SExpression(entries);
            _manager = new Macro_manager();
            _manager.LoadMacro(expression.second());
        }

        [Fact]
        public void IsMacroWorking()
        {
            string test_expression = "(multi 0 1)";
            Parser.SExpression expression = new Parser.SExpression(test_expression);
            Parser.SExpression result = _manager.FindMacro(expression);

            string expected_text = "(* 0 1)";
            Parser.SExpression expected_expression = new Parser.SExpression(expected_text);

            Assert.True(expected_expression.Equal(result), "L'execution de la macro n'est pas correcte, la macro renvoyer n'est pas celle attendue");

        }


    }
}

