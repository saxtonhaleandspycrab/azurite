using System.Collections.Generic;
using System.Linq;
using Azurite;
using Xunit;

namespace SourceTests
{
    public class Macro_Loading
    {
        // private readonly PrimeService _primeService;

        private Macro_manager _manager;
        private Parser.SExpression _expression;

        public Macro_Loading()
        {

            string entries = "(macro (multi x y) (* x y))";
            _expression = new Parser.SExpression(entries);
            _manager = new Macro_manager();
            _manager.LoadMacro(_expression.second());
        }

        [Fact]
        public void IsLoadingCorrect()
        {
            Assert.True(_manager.Macro_list.Count == 1, $"il ya {_manager.Macro_list.Count} dans manager.Macro_list au lieu de 1");
        }

        [Fact]
        public void IsMacroNameCorrect()
        {
            string name = _manager.Macro_list[0].keyword;
            Assert.True(name == "multi", $"le nom de la macro est {name} au lieu de mulit");
        }

        [Fact]
        public void IsParametersCorrect()
        {
            List<string> parameters = _manager.Macro_list[0].parameters;
            List<string> expected = new List<string>{"x", "y"};
            Assert.True(Enumerable.SequenceEqual(parameters, expected), $"The paramaters are {string.Join(' ', parameters)} intead of {string.Join(' ', expected)}");
        }

        [Fact]
        public void IsBodyCorrect()
        {
            Parser.SExpression body =_manager.Macro_list[0].body;
            Parser.SExpression expected = _expression.second().second().first();
            Assert.Equal(body, expected);
        }
    }
}