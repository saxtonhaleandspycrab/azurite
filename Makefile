all:
	mcs -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono azurite.exe

debug:
	mcs -debug -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono --debug --debugger-agent=transport=dt_socket,server=y,address=127.0.0.1:55555 azurite.exe

debug_file:
	mcs -debug -langversion:7 ./src/*.cs -out:azurite.exe
	clear && ledit mono --debug --debugger-agent=transport=dt_socket,server=y,address=127.0.0.1:55555 azurite.exe ./tests/macro.azur -t ocaml -s ./tests/compiled

build:
	mcs -langversion:7 ./src/*.cs -out:azurite.exe

run:
	clear && ledit mono azurite.exe

run_file:
	ledit mono ./azurite.exe ./tests/macro.azur -t ocaml -s ./tests/compiled

config:
	[ -d ~/.azurite ] && rm -r ~/.azurite
	mkdir ~/.azurite
	mkdir ~/.azurite/lib

install:
	cp ./azurite.exe ~/.azurite/azurite.exe
	cp -R ./stdlib ~/.azurite/stdlib
	cp ./distribution/azurite ~/.azurite/azurite
	chmod +x ~/.azurite/azurite
	chmod +x ~/.azurite/azurite.exe
	echo "PATH+=:~/.azurite/" >> ~/.bashrc

uninstall:
	rm -r ~/.azurite

install_all:
	make build
	make config
	make build
	make install
	rm azurite.exe
