# Azurite Documentation


## Les Translates
> Les "translate" représentent la base du transpileur, ils permettent de décrire comment sont transpilées les S-expressions.

*Structure d'un translate*
```lisp
(translate (nom paramètres) ("type") (langage "import" "body"))
```

*Exemple:*
```lisp
(translate ("+" a b) ("num" "num" "num") (python "" "{a} + {b}"))
```
Ici est définis comment + est convertis en python

### Les paramètres

En Azurite, les translates s'appuient sur un système de pattern matching, ainsi les paramètres doivent être déclarés d'une manière spéciale.

Symbole | Exemple | Nom | Description
--- | --- | --- | ---
" " | "=" | Match fort | Lors de la recherche le jeton doit être exactement égal
' ' | 'name' | Match strict | Lors de la recherche le jeton doit être directement présent
[ ] | [function_name] | Callable | Lors de la recherche le jeton doit pouvoir être appelé
∅| x | Match faible | Lors de la recherche le jeton peut être n’importe quoi, il peut s’agir d’une S-Expression.
∅... | x.. | List | Lors de la recherche le jeton doit être une liste

#### Les listes
Déclaration d’une liste en azurite dane les translate:
```lisp
    (translate ("list" x...) ("type") (langage "" ("{x ',' '[' ']' }")))
```
Une liste est déclarée telle que le jeton est suivi de …
Syntaxe de déclaration d’une liste, 
```lisp
    (translate ( nom...) ("type") (langage "" ("{nom 'separator' 'start' 'end' }")))
```
Start et end sont optionnel mais si besoin ils doivent forcément être précisé.
Autorisé :
```lisp
    (translate ( nom...) ("type") (langage "" ("{nom 'séparateur'}")))
```
Interdit :
```lisp
    (translate ( nom...) ("type") (langage "" ("{nom 'séparateur' 'start'}")))
```
### Le body
Le body ou le corps de l'expression est l'expression traduite dans le langage ciblé. 
Soit le translate suivant:
```lisp
(translate ("nom" 'light' x) (langage "" "si {light} faire {x}"))
```
Les paramètres sont situés entre accolade dans le corps, c'est à cet emplacement qu'ils seront remplacés.

### le typage
Les translates sont typés, pour le typage, on ne précise pas le type des Match fort exemple:

```lisp
(translate ("nom" 'num' "milieu" liste_str... ) ("num" "str..." "type_de_retour") (langage "" "do_something ..."))
```

### les types
Il existe plusieurs types de base en azurite:
- num : flottants
- str : chaînes
- bool : booléens
- unit : équivalent de void, en tant que type de retour, utilisé par exemple par print
- topunit : sert pour le type de retour d'une expression ne pouvant être placée dans une autre, par exemple une définition de fonction
- #nombre : désigne un type polymorphe d'id nombre. Tous les polymorphes au sein d'une même définition sont équivalents si leurs nombres sont équivalents
- type... : désigne une liste de type. Notez qu'une liste placée en fin de fonction ou d'expression peut-être interprétée comme un nombre variable d'arguments,suivant l'arité. Si elle est respectée, le jeton représente une liste. Sinon, tous les arguments dépassant seront considérés comme en faisant partie.

### valeur
Les expressions typées qui devront être placées dans les fonctions doivent représenter une valeur, par exemple on ne peut pas retourner une déclaration,
ainsi tous les corps de translate doivent pourvoir être typés. De plus, afin d'avoir une homoiconicité, il est préférable d'utiliser des listes.

### les imports
Il se peut qu'un translate ait besoin d'une bibliothèque externe ou d'une fonction d'aide pour fonctionner, on peut ainsi écrire ce codeen utilisant le champ import:
```lisp
(translate ("sqrt" x ) ("num" "num") (language "from Math import sqrt" "sqrt({x})"))
```

### librairie standard
Une librairie standard a déjà été prototypée, trouvable dans ./stdlib/protos.azurprotos